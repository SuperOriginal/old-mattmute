package me.supercube101;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Aaron.
 */
public class SoftmuteCommand implements CommandExecutor{
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String lbl, String[] args) {


        if(cmd.getName().equalsIgnoreCase("softmute")){

            if(!sender.hasPermission("matt.mute")){sender.sendMessage(ChatColor.RED + "No permission!"); return true;}

            if(args.length == 0){
                sender.sendMessage(ChatColor.RED + "Usage: /softmute [player] <TimeAmt> <S,M,H,D>");
                return true;
            }

            if(args.length == 1){
                Player target = Bukkit.getServer().getPlayer(args[0]);
                if(target == null){sender.sendMessage(ChatColor.RED + "Specified player is not online!"); return true;}

                if(CooldownManager.isCooling(target.getName(), "softmute")){  //If they are already muted:
                    CooldownManager.removeCooldown(target.getName(), "softmute");
                    target.sendMessage(ChatColor.GRAY + "You are no longer softmuted.");
                    tellStaff(sender.getName() + " has unsoftmuted " + target.getName() + ".");
                    return true;
                }
                if(CooldownManager.isCooling(target.getName(), "mute")) CooldownManager.removeCooldown(target.getName(), "mute"); //If they are muted, override it.

                CooldownManager.add(target.getName(), "softmute", Integer.MAX_VALUE, System.currentTimeMillis()); //mute 4 all the timez
                target.sendMessage(ChatColor.GRAY + "You have been softmuted.");
                tellStaff(sender.getName() + " has softmuted " + target.getName() + ".");
                return true;
            }

            if(args.length == 2){
                sender.sendMessage(ChatColor.RED + "Please specify a time unit! (S,M,H,D)");
                return true;
            }
            int time;

            try{
                time = Integer.parseInt(args[1]);
            }catch (NumberFormatException e){
                sender.sendMessage(ChatColor.RED + "Please specify a valid amount.");
                return true;
            }

            if(args[2].equalsIgnoreCase("s")){
                Player target = Bukkit.getServer().getPlayer(args[0]);
                if(target == null){sender.sendMessage(ChatColor.RED + "Specified player is not online!"); return true;}


                if(CooldownManager.isCooling(target.getName(), "mute")) CooldownManager.removeCooldown(target.getName(), "mute");
                if(CooldownManager.isCooling(target.getName(), "softmute")) CooldownManager.removeCooldown(target.getName(), "softmute"); //If they are softmuted, override it.

                CooldownManager.add(target.getName(), "softmute", time, System.currentTimeMillis());
                target.sendMessage(ChatColor.GRAY + "You have been softmuted for " + time + " seconds.");
                tellStaff(sender.getName() + " has softmuted " + target.getName() + " for " + time + " seconds.");
                return true;
            }

            if(args[2].equalsIgnoreCase("m")){
                Player target = Bukkit.getServer().getPlayer(args[0]);
                if(target == null){sender.sendMessage(ChatColor.RED + "Specified player is not online!"); return true;}

                if(CooldownManager.isCooling(target.getName(), "mute")) CooldownManager.removeCooldown(target.getName(), "mute");
                if(CooldownManager.isCooling(target.getName(), "softmute")) CooldownManager.removeCooldown(target.getName(), "softmute"); //If they are softmuted, override it.

                CooldownManager.add(target.getName(), "softmute", time * 60, System.currentTimeMillis());
                target.sendMessage(ChatColor.GRAY + "You have been softmuted for " + time + " minutes.");
                tellStaff(sender.getName() + " has softmuted " + target.getName() + " for " + time + " minutes.");
                return true;
            }

            if(args[2].equalsIgnoreCase("h")){
                Player target = Bukkit.getServer().getPlayer(args[0]);
                if(target == null){sender.sendMessage(ChatColor.RED + "Specified player is not online!"); return true;}

                if(CooldownManager.isCooling(target.getName(), "mute")) CooldownManager.removeCooldown(target.getName(), "mute");
                if(CooldownManager.isCooling(target.getName(), "softmute")) CooldownManager.removeCooldown(target.getName(), "softmute"); //If they are softmuted, override it.

                CooldownManager.add(target.getName(), "softmute", time * 60 * 60, System.currentTimeMillis());
                target.sendMessage(ChatColor.GRAY + "You have been softmuted for " + time + " hours.");
                tellStaff(sender.getName() + " has softmuted " + target.getName() + " for " + time + " hours.");
                return true;
            }

            if(args[2].equalsIgnoreCase("d")){
                Player target = Bukkit.getServer().getPlayer(args[0]);
                if(target == null){sender.sendMessage(ChatColor.RED + "Specified player is not online!"); return true;}

                if(CooldownManager.isCooling(target.getName(), "mute")) CooldownManager.removeCooldown(target.getName(), "mute");
                if(CooldownManager.isCooling(target.getName(), "softmute")) CooldownManager.removeCooldown(target.getName(), "softmute"); //If they are softmuted, override it.

                CooldownManager.add(target.getName(), "softmute", time * 60 * 60 * 24, System.currentTimeMillis());
                target.sendMessage(ChatColor.GRAY + "You have been softmuted for " + time + " days.");
                tellStaff(sender.getName() + " has softmuted " + target.getName() + " for " + time + " days.");
                return true;
            }
        }
        return true;
    }

    public void tellStaff(String msg){
        for(Player p : Bukkit.getOnlinePlayers()){
            if(p.hasPermission("matt.mute")){
                p.sendMessage(ChatColor.GOLD + "[SoftMute] " + ChatColor.AQUA + msg);
            }
        }
    }

}
