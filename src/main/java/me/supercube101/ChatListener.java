package me.supercube101;

import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.struct.ChatMode;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

/**
 * Created by Aaron.
 */
public class ChatListener implements Listener{

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e){

        Player p = e.getPlayer();

        if(CooldownManager.isCooling(p.getName(), "mute")) {
            e.setCancelled(true);
            p.sendMessage(ChatColor.RED + "You are muted!");
            return;
        }

        if(CooldownManager.isCooling(p.getName(), "softmute")){
            FPlayer fPlayer = FPlayers.i.get(p);

            if(fPlayer.getChatMode().equals(ChatMode.PUBLIC)){
                e.setCancelled(true);
                p.sendMessage(ChatColor.RED + "You are softmuted!");
            }
        }
    }
    @EventHandler
    public void onCmd(PlayerCommandPreprocessEvent e) {
        if(CooldownManager.isCooling(e.getPlayer().getName(), "mute")) {
            if (e.getMessage().startsWith("/msg ") || e.getMessage().startsWith("/tell ") || e.getMessage().startsWith("/t ") || e.getMessage().startsWith("/etell ")|| e.getMessage().startsWith("/emsg ")|| e.getMessage().startsWith("/et ")) {
                e.setCancelled(true);
                e.getPlayer().sendMessage(ChatColor.RED + "You are muted!");
                return;
            }
        }
    }

}
