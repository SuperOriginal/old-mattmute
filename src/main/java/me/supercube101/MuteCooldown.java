package me.supercube101;

import java.util.HashMap;

/**
 * Created by Aaron.
 */
public class MuteCooldown {
    public HashMap<String, MuteCooldown> cooldownMap = new HashMap<String, MuteCooldown>();

    String player;
    String ability = "";
    long systime;
    long sec;

    public MuteCooldown(String player){
        this.player = player;
    }

    public  MuteCooldown(String player, long sec, long systime){
        this.player = player;
        this.sec = sec;
        this.systime = systime;
    }
}
