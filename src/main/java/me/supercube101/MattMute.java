package me.supercube101;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Aaron.
 */
public class MattMute extends JavaPlugin{

    public void onEnable(){
        getCommand("mute").setExecutor(new MuteCommand());
        getCommand("softmute").setExecutor(new SoftmuteCommand());

        Bukkit.getServer().getPluginManager().registerEvents(new ChatListener(), this);

        Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {

            @Override
            public void run() {
                CooldownManager.handleCooldowns();
            }
        }, 1L, 1L);
        retrieveMutesFromConfig();
        saveDefaultConfig();
    }

    public void onDisable(){
        dumpAllMutesToConfig();
    }

    public void dumpAllMutesToConfig(){
        for(String player : CooldownManager.cooldownPlayers.keySet()){
            getConfig().createSection(player);
            for(String mute : CooldownManager.cooldownPlayers.get(player).cooldownMap.keySet()){
                getConfig().getConfigurationSection(player).set(mute, CooldownManager.getRemaining(player, mute));
            }
        }
        saveConfig();
    }

    public void retrieveMutesFromConfig(){
        for(String player : getConfig().getKeys(false)){
            for(String mute : getConfig().getConfigurationSection(player).getKeys(false)){
                CooldownManager.add(player, mute, getConfig().getConfigurationSection(player).getLong(mute), System.currentTimeMillis());
            }
        }
        for(String key : getConfig().getKeys(false)){ //Clear config.
            getConfig().set(key, null);
        }
        saveConfig();
    }

}
